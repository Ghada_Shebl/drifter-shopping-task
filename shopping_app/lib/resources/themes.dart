import 'package:flutter/material.dart';
import 'package:shopping_app/resources/colors.dart';

class AppThemes {
  static final appTheme = ThemeData(
    primaryColor: AppColors.primary,
    fontFamily: "Cabin",
  );
}
