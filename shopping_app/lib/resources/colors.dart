import 'package:flutter/material.dart';

class AppColors {
  static const primary = Color(0xFF5C6BC0);
  static const lightGray = Color(0xFFBABABA);
  static const darkGray = Color(0xFF575757);
  static const yellow = Color(0xFFFFB300);
  static const orange = Color(0xFFF46038);

}