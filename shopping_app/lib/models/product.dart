import 'package:flutter/material.dart';

class Product {
  String name;
  double rating;
  String specs;
  String zlotyPrice;
  String dollarPrice;
  String image;
  Color backgroundColor;

  Product(
      {required this.name,
      required this.rating,
      required this.specs,
      required this.zlotyPrice,
      required this.dollarPrice,
      required this.image,
      required this.backgroundColor});
}
