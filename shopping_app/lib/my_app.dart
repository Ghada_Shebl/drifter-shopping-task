import 'package:flutter/material.dart';
import 'package:shopping_app/resources/themes.dart';
import 'package:shopping_app/views/ProductsList.dart';


class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
     return Material(
      child: MaterialApp(
        home: const ProductsList(),
        theme: AppThemes.appTheme,
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
