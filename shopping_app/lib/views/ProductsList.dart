import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:shopping_app/models/product.dart';
import 'package:shopping_app/models/product_type.dart';
import 'package:shopping_app/resources/colors.dart';
import 'package:shopping_app/resources/strings.dart';

class ProductsList extends StatefulWidget {
  const ProductsList({Key? key}) : super(key: key);

  @override
  State<ProductsList> createState() => _ProductsListState();
}

class _ProductsListState extends State<ProductsList> {
  int selectedType = 0;
  List<ProductType> productTypes = [
    ProductType(title: "ALL"),
    ProductType(title: "ULTRABOOST"),
    ProductType(title: "ADILETTE"),
    ProductType(title: "ALPHABOUNCE"),
  ];

  List<Product> mainProducts = [
    Product(
      name: "SUPERSTAR SHOES",
      backgroundColor: AppColors.yellow,
      rating: 5,
      specs: "Carbon / Core Black / Solar Red",
      zlotyPrice: "₹ 11,454",
      dollarPrice: "\$ 120",
      image: "assets/images/shoe_three.png"
    ),
    Product(
        name: "SUPERSTAR SHOES",
        backgroundColor: AppColors.orange,
        rating: 5,
        specs: "Carbon / Core Black / Solar Red",
        zlotyPrice: "₹ 11,454",
        dollarPrice: "\$ 120",
        image: "assets/images/shoe_four.png"
    ),
    Product(
        name: "SUPERSTAR SHOES",
        backgroundColor: AppColors.primary,
        rating: 5,
        specs: "Carbon / Core Black / Solar Red",
        zlotyPrice: "₹ 11,454",
        dollarPrice: "\$ 120",
        image: "assets/images/shoe_one.png"
    ),
  ];
  List<Product> productOptions = [
    Product(
        name: "SUPERSTAR SHOES",
        backgroundColor: AppColors.yellow,
        rating: 5,
        specs: "Cloud White / Core Black / Solar Red",
        zlotyPrice: "₹ 12,454",
        dollarPrice: "\$ 100",
        image: "assets/images/shoe_one.png"
    ),
    Product(
        name: "SUPERSTAR SHOES",
        backgroundColor: AppColors.orange,
        rating: 5,
        specs: "Core Black / Silver Metallic / Solar Yellow",
        zlotyPrice: "₹ 11,455",
        dollarPrice: "\$ 110",
        image: "assets/images/shoe_two.png"
    ),
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:SafeArea(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  _buildAppBar(),
                  SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Container(
                        margin: const EdgeInsetsDirectional.only(start: 20, end: 16, top: 10),
                        child:  Row(
                          children: _buildFilterList(),
                        ),
                      )
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Container(
                      margin: const EdgeInsetsDirectional.all(20),
                      child: Row(
                        children: _buildMainProductsList(),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
                    child: const Text(
                      "244 OPTIONS",
                      style: TextStyle(
                          fontSize: 18,
                          color: AppColors.darkGray
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  ListView(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    children: _buildOptionsList(),
                  )
                ],
              ),
            )
        ));
  }

  Widget _buildAppBar() {
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 5, right: 5),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Row(
          children: [
            IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.arrow_back_rounded,
                color: Colors.black87,
                size: 30,
              ),
            ),
            Expanded(
              child: Container(),
            ),
            IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.search_rounded,
                color: Colors.black87,
                size: 30,
              ),
            ),
          ],
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
          child: Text(
            AppStrings.shoes,
            style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 24,
                color: Colors.black87),
          ),
        )
      ]),
    );
  }

  List <Widget> _buildFilterList() {
    List<Widget> filtersList = List.empty(growable: true);
    for (int i = 0; i < productTypes.length; i++) {
      filtersList.add(
          Container(
            margin: const EdgeInsets.only(right: 15),
            child: TextButton(
              onPressed: (){
                setState(() {
                  selectedType = i;
                });
              },
              style: TextButton.styleFrom(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                backgroundColor: i == selectedType?AppColors.darkGray: AppColors.lightGray,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(12))
                )
              ),
              child: Text(
                productTypes[i].title,
                style: TextStyle(
                    color: i == selectedType? Colors.white: AppColors.darkGray
                ),
              ),
            ),
          ));
    }
    return filtersList;
  }

  List <Widget> _buildMainProductsList(){
    List<Widget> productsList = List.empty(growable: true);
    for (int i = 0; i < mainProducts.length; i++){
      productsList.add(
        Stack(
          children: [

            Container(
              margin: const EdgeInsets.only(right: 35),
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                  color: mainProducts[i].backgroundColor,
                  borderRadius: BorderRadius.circular(25)
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        mainProducts[i].name,
                        style: const TextStyle(
                            fontSize: 20,
                            color: Colors.black87,
                            fontWeight: FontWeight.bold
                        ),
                      ),

                      const SizedBox(width: 30),

                      RatingBarIndicator(
                          rating: mainProducts[i].rating,
                          itemBuilder: (context, index) => const Icon(
                            Icons.star,
                            color: Colors.white,
                          ),
                          itemCount: 5,
                          itemSize: 15.0,
                          direction: Axis.horizontal
                      ),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 5),
                    child: Text(
                      mainProducts[i].specs,
                      style: const TextStyle(
                        color: Colors.black87,
                        fontSize: 12,
                      ),
                    ),
                  ),
                  const SizedBox(height: 15),
                  Text(
                    mainProducts[i].zlotyPrice,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 17
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 3, right: 10),
                        height: 250,
                        width: 1,
                        decoration: const BoxDecoration(
                            color: Colors.black87
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 30),
                        child: Text(
                          mainProducts[i].dollarPrice,
                          style: const TextStyle(
                              fontSize: 40
                          ),
                        ),
                      )

                    ],
                  )
                ],
              ),
            ),
            Positioned(
                bottom: 30,
                left: 60,
                child: Image.asset(
                  mainProducts[i].image,
                  width: 290,
                )),
          ],
        ),
      );
    }
    return productsList;
  }

  List <Widget> _buildOptionsList(){
    List<Widget> productsList = List.empty(growable: true);
    for (int i = 0; i < productOptions.length; i++){
      productsList.add(
        Container(
          margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
          child: Row(
            children: [
              Transform.rotate(angle: -6, child: Image.asset(
                productOptions[i].image,
                width: 100,
              )),
              const SizedBox(width: 15),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                      productOptions[i].specs
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Text(
                          productOptions[i].zlotyPrice
                      ),
                      const SizedBox(width: 60),
                      Text(
                        productOptions[i].dollarPrice,
                        style: const TextStyle(
                            fontSize: 16
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ],
          )
        )
      );
    }
    return productsList;
  }
}


